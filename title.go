package eizwho

// Title is the main structure for the `Titles` collection
type Title struct {
	Awards            []Award       `json:"Awards" bson:"Awards"`
	Genres            []string      `json:"Genres" bson:"Genres"`
	OtherNames        []OtherName   `json:"OtherNames" bson:"OtherNames"`
	Participants      []Participant `json:"Participants" bson:"Participants"`
	ReleaseYear       int           `json:"ReleaseYear" bson:"ReleaseYear"`
	Storylines        []StoryLine   `json:"Storylines" bson:"Storylines"`
	TitleID           int           `json:"TitleId" bson:"TitleId"`
	TitleName         string        `json:"TitleName" bson:"TitleName"`
	TitleNameSortable string        `json:"TitleNameSortable" bson:"TitleNameSortable"`
	_id               string
}

// Award contains details of an award won
type Award struct {
	AwardWon     bool     `json:"AwardWon" bson:"AwardWon"`
	AwardYear    int      `json:"AwardYear" bson:"AwardYear"`
	Participants []string `json:"Participants" bson:"Participants"`
	Award        string   `json:"Award" bson:"Award"`
	AwardCompany string   `json:"AwardCompany" bson:"AwardCompany"`
}

// OtherName contains details about other names by which a title may be known
type OtherName struct {
	TitleNameLanguage string `json:"TitleNameLanguage" bson:"TitleNameLanguage"`
	TitleNameType     string `json:"TitleNameType" bson:"TitleNameType"`
	TitleNameSortable string `json:"TitleNameSortable" bson:"TitleNameSortable"`
	TitleName         string `json:"TitleName" bson:"TitleName"`
}

// Participant contains details about participants in the work.
// This can be an actor, producer, editor, studio, etc., on-screen or off.
type Participant struct {
	IsKey           bool   `json:"IsKey" bson:"IsKey"`
	RoleType        string `json:"RoleType" bson:"RoleType"`
	IsOnScreen      bool   `json:"IsOnScreen" bson:"IsOnScreen"`
	ParticipantType string `json:"ParticipantType" bson:"ParticipantType"`
	Name            string `json:"Name" bson:"Name"`
	ParticipantID   int    `json:"ParticipantId" bson:"ParticipantId"`
}

// StoryLine has information about the story line.
type StoryLine struct {
	Description string `json:"Description" bson:"Description"`
	Language    string `json:"Language" bson:"Language"`
	Type        string `json:"Type" bson:"Type"`
}
