package eizwho

type ParticipantName struct {
	Name string `json:"Name" bson:"Name"`
}

// TitleParticipants is the main structure for the `Titles` collection
type TitleParticipants struct {
	Participants ParticipantName `json:"Participants" bson:"Participants"`
}
