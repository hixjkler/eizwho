package eizwho

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"reflect"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
)

// SearchByTitle uses the mongo `find` function to search for records that match
// the `titleMatch` argument.  The return is a byte slice of JSON and, when something
// fails, an error message.
func SearchByTitle(titleMatch string) (b []byte, err error) {

	collection := MongoClient.Database("dev-challenge").Collection("Titles")
	filter := bson.M{"TitleName": bson.M{"$regex": fmt.Sprintf("%s", titleMatch)}}

	ctx := context.Background()
	var cur *mongo.Cursor
	cur, err = collection.Find(ctx, filter) //bson.D{})
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		// decode result into Title struct:
		title := Title{}
		errDecode := cur.Decode(&title)
		if errDecode != nil {
			err = errDecode
			return
		}

		// marshal data into JSON;
		bresult, errMarshal := json.Marshal(title)
		if errMarshal != nil {
			err = errMarshal
			return
		}

		b = append(b, bresult...)
	}
	if err = cur.Err(); err != nil {
		return
	}

	return
}

type FieldSearch struct {
	Collection *mongo.Collection
	Field      string
	FieldMatch interface{}
}

// SearchByField uses the mongo `find` function to search for records that match the `field` field
// with the `fieldMatch` string.  The result is then marshalled into JSON and returned as a byte
// slice of JSON.  When something fails, an error is returned.
func SearchByField(ctx context.Context, fs FieldSearch) (b []byte, err error) {

	filter := bson.M{}
	if len(fs.Field) > 0 {
		switch reflect.TypeOf(fs.FieldMatch) {
		case reflect.TypeOf(1):
			filter = bson.M{fs.Field: fs.FieldMatch}
		case reflect.TypeOf("1"):
			filter = bson.M{fs.Field: bson.M{"$regex": fmt.Sprintf("%s", fs.FieldMatch)}}
		case reflect.TypeOf(1.0):
			// float values obviously needs more thought than this:
			filter = bson.M{fs.Field: fs.FieldMatch}
		}
	}

	var cur *mongo.Cursor
	cur, err = fs.Collection.Find(ctx, filter)
	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		title := Title{}
		// decode result into result struct:
		errDecode := cur.Decode(&title)
		if errDecode != nil {
			err = errDecode
			return
		}

		// marshal data into JSON;
		bresult, errMarshal := json.Marshal(title)
		if errMarshal != nil {
			err = errMarshal
			return
		}

		b = append(b, bresult...)
	}
	if err = cur.Err(); err != nil {
		return
	}

	return

} // SearchByField

// SearchByPipeline uses the mongo's aggregate pipeline feature to search for records that survive
// the pipeline flow.  The result is then marshalled into JSON and returned as a byte slice of JSON.
// When something fails, an error is returned.
func SearchByPipeline(ctx context.Context, collection *mongo.Collection, pipeline interface{}) (b []byte, err error) {

	var cur *mongo.Cursor
	cur, err = collection.Aggregate(ctx, pipeline)

	if err != nil {
		log.Fatal(err)
	}
	defer cur.Close(ctx)

	//var result TitleParticipants
	//fmt.Println(reflect.TypeOf(result))
	for cur.Next(ctx) {
		//fmt.Println(cur.Current)
		result := TitleParticipants{}

		// decode result into result struct:
		errDecode := cur.Decode(&result)
		if errDecode != nil {
			err = errDecode
			return
		}

		//fmt.Println(result.Participants.Name)

		// marshal data into JSON;
		bresult, errMarshal := json.Marshal(result)
		if errMarshal != nil {
			err = errMarshal
			return
		}

		b = append(b, bresult...)
	}
	if err = cur.Err(); err != nil {
		return
	}

	return

} // SearchByPipeline
