package main

import (
	"context"
	"eizwho"
	"fmt"

	"github.com/mongodb/mongo-go-driver/bson"
)

var (
	localDBC = eizwho.DBConnect{
		Host:   "localhost",
		Port:   27017,
		DBName: "dev-challenge",
	}

	turnerDBC = eizwho.DBConnect{
		Host:     "ds043348.mongolab.com",
		Port:     43348,
		User:     "readonly",
		Password: "turner",
		DBName:   "dev-challenge",
	}
)

func main() {

	eizwho.DBC = localDBC
	//eizwho.DBC = turnerDBC

	errConnect := eizwho.ConnectDB()
	if errConnect != nil {
		fmt.Println(errConnect)
		return
	}
	mc := eizwho.MongoClient
	collection := mc.Database("dev-challenge").Collection("Titles")
	//fmt.Println(mc)

	// titleSearch := "Disturb"
	// title, errSearch := eizwho.SearchByTitle(titleSearch)
	// if errSearch != nil {
	// 	fmt.Println("errSearch:  ", errSearch)
	// }
	// fmt.Println(string(title))

	// field := "ReleaseYear"
	// fieldMatch := 1998
	// fs := eizwho.FieldSearch{
	// 	Collection: collection,
	// 	Field:      field,
	// 	FieldMatch: fieldMatch,
	// }

	// result, errSearch := eizwho.SearchByField(context.Background(), fs)
	// if errSearch != nil {
	// 	fmt.Println("errSearch:  ", errSearch)
	// }
	// fmt.Println(string(result))

	titleName := "Disturbia"
	pipeline := []bson.M{
		bson.M{"$match": bson.M{"TitleName": titleName}},
		bson.M{"$unwind": "$Participants"},
		bson.M{"$match": bson.M{"Participants.IsOnScreen": true}},
		bson.M{"$project": bson.M{"_id": 0, "Participants.Name": 1}},
	}

	result, errSearch := eizwho.SearchByPipeline(context.Background(), collection, pipeline)
	if errSearch != nil {
		fmt.Println("errSearch:  ", errSearch)
	}
	fmt.Println(string(result))

	return

}
