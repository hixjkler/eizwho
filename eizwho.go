package eizwho

import (
	"context"
	"fmt"
	"time"

	"github.com/mongodb/mongo-go-driver/mongo"
	"github.com/mongodb/mongo-go-driver/mongo/readpref"
)

var (
	// MongoClient is the global client variable for the mongo database
	MongoClient *mongo.Client

	// DBC contains the DBConnect data needed to connect to the database
	DBC DBConnect
)

// DBConnect contains all of the parameters needed to connect to a mongo database
// The mongo documentation (https://docs.mongodb.com/manual/reference/connection-string/)
// declares the URI connection scheme to be this:
//
// mongodb://[username:password@]host1[:port1][,...hostN[:portN]]][/[database][?options]]
//
// Todo:  add options capabilities to connection URI
type DBConnect struct {
	// Host is mandatory name of host
	Host string `json:"host"`

	// Port is mandatory port through which to connect on db server
	Port int `json:"port"`

	// User is an optional db user name
	User string `json:"user"`

	// Password is an optional db user password
	Password string `json:"password"`

	// DBName is the optional name of the database to which a connection will be made
	DBName string `json:"dbname"`
}

func init() {
}

// ConnectDB is the exported routine for performing the database connection
func ConnectDB() error {
	return dbConnect()
}

func buildConnectionString() string {

	db := fmt.Sprintf("%s", DBC.Host)

	// connection string qualified with credentials if included in DBC:
	if len(DBC.User) > 0 && len(DBC.Password) > 0 {
		db = fmt.Sprintf("mongodb://%s:%s@%s", DBC.User, DBC.Password, db)
	} else {
		db = fmt.Sprintf("mongodb://%s", db)
	}

	// connection string specified with a port if included in DBC:
	if DBC.Port > 0 {
		db = fmt.Sprintf("%s:%d", db, DBC.Port)
	}

	// connection string specified with a dbname if included in DBC:
	if len(DBC.DBName) > 0 {
		db = fmt.Sprintf("%s/%s", db, DBC.DBName)
	}

	return db
}

func dbConnect() (err error) {

	db := buildConnectionString()

	MongoClient, err = mongo.NewClient(db)
	if err != nil {
		return err
	}

	// try to connect within a set time limit:
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = MongoClient.Connect(ctx)
	if err != nil {
		return err
	}

	return nil
}

// Ping is used to test for a db connection
func Ping(client *mongo.Client, t int) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(t)*time.Second)
	defer cancel()
	err := client.Ping(ctx, readpref.Primary())
	if err != nil {
		return err
	}

	return nil
}
