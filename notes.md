
## Connecting to DB

### postgres
```
psql postgres://readonly_user:turner@aws-us-east-1-portal.27.dblayer.com:25183/titles
```
This connection is blocked and that could be for any number of reasons beyond my control.
Also tried was the same connection string with `readonly` instead of `readonly_user` to
match the connection string user parameter for the mongo db:  still no response.

### mongo
While the postgres link is non-functioning, the mongo DB link seems live:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge
```
Let's try to dump the mongo data then try to import into postgres.
```
mongodump -host=ds043348.mongolab.com --port=43348 -db=dev-challenge -u readonly -p turner
```
```
2019-02-07T22:47:03.371-0500	Failed: error connecting to db server: no reachable servers
```
Can't dump data from remote.

Query all results from db:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval \
  "db.Titles.find().forEach(function(f){print(tojson(f, '', true));})" > titles.json
```
Then import into local db:
```
mongoimport --db dev-challenge --collection Titles titles.json 
```


## Let's work through some mongo basics

### querying from within the mongo shell.
```
mongo
```
List all collections:
```
show collections
```
```text
Titles
system.indexes
system.users
```
List all contents of collection Titles:
```
db.Titles.find()
```

### querying via the command line
It will be more convenient to query the db from the shell.

+ get the stats on `Titles`:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval "db.Titles.stats()" 
```
```json
{
	"ns" : "dev-challenge.Titles",
	"size" : 1318512,
	"count" : 25,
	"avgObjSize" : 52740,
	"numExtents" : 3,
	"storageSize" : 2236416,
	"lastExtentSize" : 2097152,
	"paddingFactor" : 1,
	"paddingFactorNote" : "paddingFactor is unused and unmaintained in 3.0. It remains hard coded to 1.0 for compatibility only.",
	"userFlags" : 1,
	"capped" : false,
	"nindexes" : 1,
	"totalIndexSize" : 8176,
	"indexSizes" : {
		"_id_" : 8176
	},
	"ok" : 1,
	"operationTime" : Timestamp(1549632919, 1),
	"$clusterTime" : {
		"clusterTime" : Timestamp(1549632919, 1),
		"signature" : {
			"hash" : BinData(0,"6jxdpQnYFAeB7aP3b7ozCUo9RpY="),
			"keyId" : NumberLong("6628777319769047041")
		}
	}
}
```

## some "simple" and obvious queries (learning mode)

### details of the data set
Query the get the first entry in the `Titles` collection to infer structure:

```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval "db.Titles.findOne()"
```
```json
{
	"Awards" : [
		{
			"AwardWon" : false,
			"AwardYear" : 1933,
			"Participants" : [
				"Diana Wynyard"
			],
			"Award" : "Best Actress",
			"AwardCompany" : "Academy Award"
		},
		{
			"AwardWon" : true,
			"AwardYear" : 1933,
			"Award" : "Best Art Direction",
			"AwardCompany" : "Academy Award"
		},
		{
			"AwardWon" : true,
			"AwardYear" : 1933,
			"Participants" : [
				"Frank Lloyd"
			],
			"Award" : "Best Director",
			"AwardCompany" : "Academy Award"
		},
		{
			"AwardWon" : true,
			"AwardYear" : 1933,
			"Award" : "Best Picture",
			"AwardCompany" : "Academy Award"
		}
	],
	"Genres" : [
		"Adaptation",
		"Based-on",
		"Drama"
	],
	"OtherNames" : [
		{
			"TitleNameLanguage" : "ENGLISH",
			"TitleNameType" : "Primary",
			"TitleNameSortable" : "Cavalcade",
			"TitleName" : "Cavalcade"
		}
	],
	"Participants" : [
		{
			"IsKey" : false,
			"RoleType" : "Actor",
			"IsOnScreen" : true,
			"ParticipantType" : "Person",
			"Name" : "Dick Henderson Jr.",
			"ParticipantId" : 1027760
		},
		{
			"IsKey" : false,
			"RoleType" : "Actor",
			"IsOnScreen" : true,
			"ParticipantType" : "Person",
			"Name" : "Frank Lawton",
			"ParticipantId" : 110041
		},
... (lots of entries here)
		{
			"IsKey" : false,
			"RoleType" : "Actor",
			"IsOnScreen" : true,
			"ParticipantType" : "Person",
			"Name" : "Ursula Jeans",
			"ParticipantId" : 94585
		}
	],
	"ReleaseYear" : 1933,
	"Storylines" : [
		{
			"Description" : "Tracing thirty years in the life of a British family, this is the story of a couple whose love faces the events of the turn of the twentieth century. The story begins with the Boer War, and goes on to record the death of Queen Victoria, the sinking of the Titanic, World War I and the birth of jazz.",
			"Language" : "ENGLISH",
			"Type" : "Baseline"
		},
		{
			"Description" : "A cavalcade of English life from New Year's Eve 1899 until 1933 seen through the eyes of well-to-do Londoners Jane and Robert Marryot. Amongst events touching their family are the Boer War, the death of Queen Victoria, the sinking of the Titanic and the Great War.",
			"Language" : "ENGLISH",
			"Type" : "IMDB"
		},
		{
			"Description" : "A British family survives war and changing times.",
			"Language" : "ENGLISH",
			"Type" : "Turner External"
		},
		{
			"Description" : "Tracing thirty years in the life of a British family, this is the story of a couple whose love faces the events of the turn of the twentieth century. The story begins with the Boer War, and goes on to record the death of Queen Victoria, the sinking of the Titanic, World War I and the birth of jazz.",
			"Language" : "ENGLISH",
			"Type" : "Turner Internal"
		}
	],
	"TitleId" : 70523,
	"TitleName" : "Cavalcade",
	"TitleNameSortable" : "Cavalcade",
	"_id" : "534c60c2bc028401c08db897"
}
```

From this, an estimate of the data structure is this:
Outer structure of data:
+ Awards : []Award
+ Genres : []string
+ OtherNames : []OtherName
+ Participants : []Participant
+ ReleaseYear : int
+ Storylines : []StoryLine
+ TitleId : int
+ TitleName : string
+ TitleNameSortable : string
+ _id : string

Inner substructures are these:
+ Award
  + AwardWon : bool
  + AwardYear : int
  + Participants : []string
  + Award : string
  + AwardCompany : string

+ OtherName
  + TitleNameLanguage : string
  + TitleNameType : string
  + TitleNameSortable : string
  + TitleName string

+ Participant
  + IsKey : bool
  + RoleType : string
  + IsOnScreen : bool
  + ParticipantType : string
  + Name : string
  + ParticipantId : int

+ StoryLine
  + Description : string
  + Language : string
  + Type : string


### Possible queries for given `TitleName`:
+ obvious top level properties, e.g., Awards, Genres, Participants with { IsOnScreen : true }
+ which other titles were released in that ReleaseYear?
+ which on-screen participants have worked together in other movies?
+ which on-screen participants have won awards for this or other titles?


#### List all title names in collection:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.distinct("TitleName")'
```
```
[
	"Cavalcade",
	"All About Eve",
	"All Quiet On The Western Front",
	"All the King's Men",
	"Amadeus (Part 1)",
	"2 Fast 2 Furious",
	"Amadeus (Part 2)",
	"American Beauty",
	"An American in Paris",
	"Angels & Demons",
	"Annie Hall",
	"Around The World In 80 Days",
	"Ben-Hur",
	"Ben-Hur (Part 1)",
	"Braveheart",
	"Casablanca",
	"Chariots Of Fire",
	"Cimarron",
	"Dances With Wolves",
	"Deer Hunter, The (Part 1)",
	"Disturbia",
	"Driving Miss Daisy",
	"Forrest Gump",
	"Four Brothers",
	"A Man for All Seasons"
]
```

#### List all content for given title name:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.find({"TitleName": "Cavalcade"})' | jq .
#mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.find({"TitleName": "Disturbia"}, { "Genres" : 1 })' | jq .
```
```json
{
  "Awards": [
    {
      "AwardWon": false,
      "AwardYear": 1933,
      "Participants": [
        "Diana Wynyard"
      ],
      "Award": "Best Actress",
      "AwardCompany": "Academy Award"
    },
    {
      "AwardWon": true,
      "AwardYear": 1933,
      "Award": "Best Art Direction",
      "AwardCompany": "Academy Award"
    },
    {
      "AwardWon": true,
      "AwardYear": 1933,
      "Participants": [
        "Frank Lloyd"
      ],
      "Award": "Best Director",
      "AwardCompany": "Academy Award"
    },
    {
      "AwardWon": true,
      "AwardYear": 1933,
      "Award": "Best Picture",
      "AwardCompany": "Academy Award"
    }
  ],
  "Genres": [
    "Adaptation",
    "Based-on",
    "Drama"
  ],
  "OtherNames": [
    {
      "TitleNameLanguage": "ENGLISH",
      "TitleNameType": "Primary",
      "TitleNameSortable": "Cavalcade",
      "TitleName": "Cavalcade"
    }
  ],
  "Participants": [
    {
      "IsKey": false,
      "RoleType": "Actor",
      "IsOnScreen": true,
      "ParticipantType": "Person",
      "Name": "Dick Henderson Jr.",
      "ParticipantId": 1027760
    },
... (lots of entries here)
    {
      "IsKey": false,
      "RoleType": "Actor",
      "IsOnScreen": true,
      "ParticipantType": "Person",
      "Name": "Ursula Jeans",
      "ParticipantId": 94585
    }
  ],
  "ReleaseYear": 1933,
  "Storylines": [
    {
      "Description": "Tracing thirty years in the life of a British family, this is the story of a couple whose love faces the events of the turn of the twentieth century. The story begins with the Boer War, and goes on to record the death of Queen Victoria, the sinking of the Titanic, World War I and the birth of jazz.",
      "Language": "ENGLISH",
      "Type": "Baseline"
    },
    {
      "Description": "A cavalcade of English life from New Year's Eve 1899 until 1933 seen through the eyes of well-to-do Londoners Jane and Robert Marryot. Amongst events touching their family are the Boer War, the death of Queen Victoria, the sinking of the Titanic and the Great War.",
      "Language": "ENGLISH",
      "Type": "IMDB"
    },
    {
      "Description": "A British family survives war and changing times.",
      "Language": "ENGLISH",
      "Type": "Turner External"
    },
    {
      "Description": "Tracing thirty years in the life of a British family, this is the story of a couple whose love faces the events of the turn of the twentieth century. The story begins with the Boer War, and goes on to record the death of Queen Victoria, the sinking of the Titanic, World War I and the birth of jazz.",
      "Language": "ENGLISH",
      "Type": "Turner Internal"
    }
  ],
  "TitleId": 70523,
  "TitleName": "Cavalcade",
  "TitleNameSortable": "Cavalcade",
  "_id": "534c60c2bc028401c08db897"
}
```

#### List the participants in a given title name:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.find({"TitleName": "Cavalcade"}, { "Participants" : 1 })' | jq .
```
```json
{
  "Participants": [
    {
      "IsKey": false,
      "RoleType": "Actor",
      "IsOnScreen": true,
      "ParticipantType": "Person",
      "Name": "Dick Henderson Jr.",
      "ParticipantId": 1027760
    },
    {
      "IsKey": false,
      "RoleType": "Actor",
      "IsOnScreen": true,
      "ParticipantType": "Person",
      "Name": "Frank Lawton",
      "ParticipantId": 110041
    },
... (lots of entries here)
    {
      "IsKey": false,
      "RoleType": "Actor",
      "IsOnScreen": true,
      "ParticipantType": "Person",
      "Name": "Ursula Jeans",
      "ParticipantId": 94585
    }
  ],
  "_id": "534c60c2bc028401c08db897"
}
```


#### List only the participant names for a given title name:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.find({"TitleName": "Cavalcade"}, { "Participants" : 1, "Participants.Name" : 1 })' | jq .
```
```json
{
  "Participants": [
    {
      "Name": "Dick Henderson Jr."
    },
    {
      "Name": "Frank Lawton"
    },
    {
      "Name": "Sonya Levien"
    },
...
    {
      "Name": "20th Century Fox Distribution"
    },
    {
      "Name": "Ursula Jeans"
    }
  ],
  "_id": "534c60c2bc028401c08db897"
}
```
List count of participant names for a given title name:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval 'db.Titles.aggregate({$project: { count: { $size:"$foo" }}})'
find({"TitleName": "Cavalcade"}, { "Participants" : 1, "Participants.Name" : 1 }).length' | jq .
```

#### List only the on-screen participant names for a given title name:
More than a dozen attempts have been made at this.  Apparently, this very simple operation maps to some very complex syntax in mongo.
I'll have to leave it alone.

#### List the academy awards for a given title name:
```
mongo mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge --quiet --eval \
'db.Titles.find(
  {"TitleName": "Cavalcade"}, 
  { "Awards" : 1 }
  )
' | jq .
```
Ideally, it would also be nice to break this down by the "AwardWon" boolean, i.e., 
'won' vs. 'nominated'; however, see problem from preceding query.


Let's write some go code to get warmed up.
