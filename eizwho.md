

# eizwho
`import "eizwho"`

* [Overview](#pkg-overview)
* [Index](#pkg-index)
* [Subdirectories](#pkg-subdirectories)

## <a name="pkg-overview">Overview</a>



## <a name="pkg-index">Index</a>
* [func ConnectDB() error](#ConnectDB)
* [func Ping(client *mongo.Client, t int) error](#Ping)
* [func SearchByField(ctx context.Context, fs FieldSearch) (b []byte, err error)](#SearchByField)
* [func SearchByPipeline(ctx context.Context, collection *mongo.Collection, pipeline interface{}) (b []byte, err error)](#SearchByPipeline)
* [func SearchByTitle(titleMatch string) (b []byte, err error)](#SearchByTitle)
* [type Award](#Award)
* [type DBConnect](#DBConnect)
* [type FieldSearch](#FieldSearch)
* [type OtherName](#OtherName)
* [type Participant](#Participant)
* [type ParticipantName](#ParticipantName)
* [type StoryLine](#StoryLine)
* [type Title](#Title)
* [type TitleParticipants](#TitleParticipants)


#### <a name="pkg-files">Package files</a>
[eizwho.go](/src/eizwho/eizwho.go) [queries.go](/src/eizwho/queries.go) [title.go](/src/eizwho/title.go) [views.go](/src/eizwho/views.go) 





## <a name="ConnectDB">func</a> [ConnectDB](/src/target/eizwho.go?s=1247:1269#L38)
``` go
func ConnectDB() error
```
ConnectDB is the exported routine for performing the database connection



## <a name="Ping">func</a> [Ping](/src/target/eizwho.go?s=2290:2334#L87)
``` go
func Ping(client *mongo.Client, t int) error
```
Ping is used to test for a db connection



## <a name="SearchByField">func</a> [SearchByField](/src/target/queries.go?s=1501:1578#L54)
``` go
func SearchByField(ctx context.Context, fs FieldSearch) (b []byte, err error)
```
SearchByField uses the mongo `find` function to search for records that match the `field` field
with the `fieldMatch` string.  The result is then marshalled into JSON and returned as a byte
slice of JSON.  When something fails, an error is returned.



## <a name="SearchByPipeline">func</a> [SearchByPipeline](/src/target/queries.go?s=2771:2887#L105)
``` go
func SearchByPipeline(ctx context.Context, collection *mongo.Collection, pipeline interface{}) (b []byte, err error)
```
SearchByPipeline uses the mongo's aggregate pipeline feature to search for records that survive
the pipeline flow.  The result is then marshalled into JSON and returned as a byte slice of JSON.
When something fails, an error is returned.



## <a name="SearchByTitle">func</a> [SearchByTitle](/src/target/queries.go?s=364:423#L7)
``` go
func SearchByTitle(titleMatch string) (b []byte, err error)
```
SearchByTitle uses the mongo `find` function to search for records that match
the `titleMatch` argument.  The return is a byte slice of JSON and, when something
fails, an error message.




## <a name="Award">type</a> [Award](/src/target/title.go?s=801:1119#L8)
``` go
type Award struct {
    AwardWon     bool     `json:"AwardWon" bson:"AwardWon"`
    AwardYear    int      `json:"AwardYear" bson:"AwardYear"`
    Participants []string `json:"Participants" bson:"Participants"`
    Award        string   `json:"Award" bson:"Award"`
    AwardCompany string   `json:"AwardCompany" bson:"AwardCompany"`
}
```
Award contains details of an award won










## <a name="DBConnect">type</a> [DBConnect](/src/target/eizwho.go?s=714:1152#L17)
``` go
type DBConnect struct {
    // Host is mandatory name of host
    Host string `json:"host"`

    // Port is mandatory port through which to connect on db server
    Port int `json:"port"`

    // User is an optional db user name
    User string `json:"user"`

    // Password is an optional db user password
    Password string `json:"password"`

    // DBName is the optional name of the database to which a connection will be made
    DBName string `json:"dbname"`
}
```
DBConnect contains all of the parameters needed to connect to a mongo database
The mongo documentation (<a href="https://docs.mongodb.com/manual/reference/connection-string/">https://docs.mongodb.com/manual/reference/connection-string/</a>)
declares the URI connection scheme to be this:

mongodb://[username:password@]host1[:port1][,...hostN[:portN]]][/[database][?options]]

Todo:  add options capabilities to connection URI


``` go
var (
    // MongoClient is the global client variable for the mongo database
    MongoClient *mongo.Client

    // DBC contains the DBConnect data needed to connect to the database
    DBC DBConnect
)
```









## <a name="FieldSearch">type</a> [FieldSearch](/src/target/queries.go?s=1140:1240#L45)
``` go
type FieldSearch struct {
    Collection *mongo.Collection
    Field      string
    FieldMatch interface{}
}
```









## <a name="OtherName">type</a> [OtherName](/src/target/title.go?s=1199:1512#L17)
``` go
type OtherName struct {
    TitleNameLanguage string `json:"TitleNameLanguage" bson:"TitleNameLanguage"`
    TitleNameType     string `json:"TitleNameType" bson:"TitleNameType"`
    TitleNameSortable string `json:"TitleNameSortable" bson:"TitleNameSortable"`
    TitleName         string `json:"TitleName" bson:"TitleName"`
}
```
OtherName contains details about other names by which a title may be known










## <a name="Participant">type</a> [Participant](/src/target/title.go?s=1653:2042#L26)
``` go
type Participant struct {
    IsKey           bool   `json:"IsKey" bson:"IsKey"`
    RoleType        string `json:"RoleType" bson:"RoleType"`
    IsOnScreen      bool   `json:"IsOnScreen" bson:"IsOnScreen"`
    ParticipantType string `json:"ParticipantType" bson:"ParticipantType"`
    Name            string `json:"Name" bson:"Name"`
    ParticipantID   int    `json:"ParticipantId" bson:"ParticipantId"`
}
```
Participant contains details about participants in the work.
This can be an actor, producer, editor, studio, etc., on-screen or off.










## <a name="ParticipantName">type</a> [ParticipantName](/src/target/views.go?s=16:86#L1)
``` go
type ParticipantName struct {
    Name string `json:"Name" bson:"Name"`
}
```









## <a name="StoryLine">type</a> [StoryLine](/src/target/title.go?s=2095:2280#L36)
``` go
type StoryLine struct {
    Description string `json:"Description" bson:"Description"`
    Language    string `json:"Language" bson:"Language"`
    Type        string `json:"Type" bson:"Type"`
}
```
StoryLine has information about the story line.










## <a name="Title">type</a> [Title](/src/target/title.go?s=75:757#L1)
``` go
type Title struct {
    Awards            []Award       `json:"Awards" bson:"Awards"`
    Genres            []string      `json:"Genres" bson:"Genres"`
    OtherNames        []OtherName   `json:"OtherNames" bson:"OtherNames"`
    Participants      []Participant `json:"Participants" bson:"Participants"`
    ReleaseYear       int           `json:"ReleaseYear" bson:"ReleaseYear"`
    Storylines        []StoryLine   `json:"Storylines" bson:"Storylines"`
    TitleID           int           `json:"TitleId" bson:"TitleId"`
    TitleName         string        `json:"TitleName" bson:"TitleName"`
    TitleNameSortable string        `json:"TitleNameSortable" bson:"TitleNameSortable"`
    // contains filtered or unexported fields
}
```
Title is the main structure for the `Titles` collection










## <a name="TitleParticipants">type</a> [TitleParticipants](/src/target/views.go?s=159:264#L1)
``` go
type TitleParticipants struct {
    Participants ParticipantName `json:"Participants" bson:"Participants"`
}
```
TitleParticipants is the main structure for the `Titles` collection














- - -
Generated by [godoc2md](http://godoc.org/github.com/davecheney/godoc2md)
